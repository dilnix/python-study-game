import math
import turtle
import random
# import airplane

window = turtle.Screen()
window.setup(1200 + 3, 800 + 3)
window.bgpic(picname='images/background.png')
window.screensize(1200, 800)
# window.tracer(n=2)

BASE_X, BASE_Y = 0, -320
ENM_X, ENM_Y = random.uniform(-580, 580), 380
BASE_ZONE_X, BASE_ZONE_Y = random.uniform(-60, 60), random.uniform(-380, -280)

def calc_heading(x1, y1, x2, y2):
    dx = x2 - x1
    dy = y2 - y1
    length = (dx ** 2 + (dy) ** 2) ** 0.5
    cos_alpha = dx / length
    alpha = math.acos(cos_alpha)
    alpha = math.degrees(alpha)
    if dy < 0:
        alpha = -alpha
    return alpha

def fire_missile(x, y):
    missile = turtle.Turtle(visible=False)
    missile.speed(0)
    missile.color('white')
    missile.penup()
    missile.setpos(x=BASE_X, y=BASE_Y)
    missile.pendown()
    heading = calc_heading(x1=BASE_X, y1=BASE_Y, x2=x, y2=y)
    missile.setheading(heading)
    missile.showturtle()
    info = {
        'missile': missile,
        'target': [x, y],
        'state': 'launched',
        'radius': 0
        }
    our_missiles.append(info)

def enemy_attack(x, y):
    attack = turtle.Turtle(visible=False)
    attack.speed(0)
    attack.color('red')
    attack.penup()
    attack.setpos(x=ENM_X, y=ENM_Y)
    attack.pendown()
    targeting = calc_heading(x1=ENM_X, y1=ENM_Y, x2=x, y2=y)
    attack.setheading(targeting)
    attack.showturtle()
    track = {
        'attack': attack,
        'target': [x, y],
        'state': 'launched',
        'radius': 0
    }
    attacks.append(track)

for track in attacks:
        state = track['state']
        attack = track['attack']
        if state == 'launched':
            attack.forward(4)
            target = track['target']
            if attack.distance(x=target[0], y=target[1]) < 10:
                track['state'] = 'explode'
                attack.shape('circle')
        elif state == 'explode':
            track['radius'] += 1
            if track['radius'] > 5:
                attack.clear()
                attack.hideturtle()
                track['state'] = 'dead'
            else:
                attack.shapesize(track['radius'])

dead_attacks = [track for track in attacks if track['state'] == 'dead']
for dead in dead_attacks:
    attack.remove(dead)

window.onclick(fire_missile)
window.ontimer(enemy_attack(BASE_ZONE_X, BASE_ZONE_Y), 2000)

our_missiles = []
attacks = []

while True:
    window.update()

    for info in our_missiles:
        state = info['state']
        missile = info['missile']
        if state == 'launched':
            missile.forward(4)
            target = info['target']
            if missile.distance(x=target[0], y=target[1]) < 20:
                info['state'] = 'explode'
                missile.shape('circle')
        elif state == 'explode':
            info['radius'] += 1
            if info['radius'] > 5:
                missile.clear()
                missile.hideturtle()
                info['state'] = 'dead'
            else:
                missile.shapesize(info['radius'])

    dead_missiles = [info for info in our_missiles if info['state'] == 'dead']
    for dead in dead_missiles:
        our_missiles.remove(dead)

    
